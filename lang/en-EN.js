export default {
  signIn: 'Sign in',
  signUp: 'Sign up',
  signInError: 'Login or password wrong',
  name: 'Name',
  date: 'Date',
  'Loading data': 'Loading data',
  'Sport type': 'Type of sport',
  signUpSuccess: 'Account successfully created',
}
