export default {
  signIn: 'Kirish',
  signUp: "Ro'yxatdan o'tish",
  signInError: "Login yoki parol noto'g'ri",
  name: 'Nomi',
  date: 'Sanasi',
  'Loading data': 'Ma`lumot yuklanmoqda',
  'Sport type': 'Sport turi',
  signUpSuccess: 'Akkaunt muvofaqqiyatli yaratildi',
}
