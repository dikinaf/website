import find from 'lodash/find'
import uniqueId from 'lodash/uniqueId'

export const state = () => ({})

export const actions = {
  loginAction(_, form) {
    const users = this.$cookies.get('users')
    const { login, password } = form
    const user = find(users, { login, password })
    return new Promise((resolve, reject) => {
      if (user) {
        resolve({ user })
        this.$cookies.set('token', user.id)
        // eslint-disable-next-line prefer-promise-reject-errors
      } else reject('Wrong password or user not exists')
    })
  },
  registerNewUserAction(_, params) {
    return new Promise((resolve, reject) => {
      const users = this.$cookies.get('users') || []
      const { login, password } = params
      const user = find(users, { login, password })
      if (!user) {
        users.push({ ...params, id: uniqueId() })
        this.$cookies.set('users', users)
        resolve('User successfully created')
        // eslint-disable-next-line prefer-promise-reject-errors
      } else reject("Can't create user")
    })
  },
}
