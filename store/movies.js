import get from 'lodash/get'

export const state = () => ({})

export const actions = {
  searchMovieAction(_, params) {
    const query = get(this, ['$router', 'history', 'current', 'query'])
    return this.$api.get('search/movie', { params: { ...query, ...params } })
  },
  getMovieListAction(_, params) {
    const query = get(this, ['$router', 'history', 'current', 'query'])
    return this.$api.get('trending/all/week', {
      params: { ...query, ...params },
    })
  },
  getMovieDetailAction(id) {
    return this.$api.get(`movie/${id}`)
  },
}
