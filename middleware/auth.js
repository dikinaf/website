export default function ({ redirect, app, route }) {
  if (!app.$cookies.get('token') && route.name !== 'login') {
    return redirect('/login')
  }
}
