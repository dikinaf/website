export default function ({ $axios, i18n }, inject) {
  // Create a custom axios instance
  const api = $axios.create()
  api.onRequest((config) => {
    config.params.api_key = process.env.API_TOKEN
    config.params.language = i18n.locale
    return config
  })

  // Set baseURL to something different
  api.setBaseURL(process.env.API_URL)

  // Inject to context as $api
  inject('api', api)
}
